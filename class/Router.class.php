<?php
/** @file Router.class.php
 *
 * @description
 * router
 * see http://www.phpro.org/tutorials/Model-View-Controller-MVC.html#8
 *
 *
 *  STILL SORTING OUT THE BEST APPROACH TO WHAT HAPPENS HERE
 *
 *
 *  MAJOR TODO
 *
 *
 *
 *
 */
class Router {
    /**
    * @the registry
    */
    private $registry;
    /**
    * @the controller path
    */
    private $path;
    private $args = array();
    public $file;
    public $controller;
    public $action;
    function __construct($registry) {
        $this->registry = $registry;
    }
    /**
     *
     * @set controller directory path
     * @param string $path
     * @return void
     */
    function setPath($path) {
        /*** check if path is a directory ***/
        if (is_dir($path) == false) {
                throw new Exception ('Invalid controller path: `' . $path . '`');
        }
        /*** set the path ***/
        $this->path = $path;
    }

}
